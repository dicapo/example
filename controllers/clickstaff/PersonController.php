<?php
class PersonController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','viewan'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','view','admin','foto','test','img_crop'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionFoto($id_kyma)
	{
		$this->layout = "foto";
		$person=Person::model()->find('id_kyma=:id_kyma', array(':id_kyma'=>$id_kyma));

		$src = 'http://db.kyma.ru/data/images/'.$person->file_name;
        $size = getimagesize ($src);
		$width_real = $size[0];
		$height_real = $size[1];
		
		if(isset($_POST['x1']))
		{
			$dest = 'upload_pic/'.$person->file_name;
			$x = $_POST["x1"];
			$y = $_POST["y1"];
			$width = $_POST["w"];
			$height = $_POST["h"];
			$this->img_crop($src, $dest, $x, $y, $width, $height);

			$foto = Newfoto::model()->find('id_kyma=:id_kyma', array(':id_kyma'=>$id_kyma));
	
			if(!isset($foto)){
				$foto = new Newfoto;
			}
			
			$foto->id_person = $person->id;
			$foto->id_kyma = $person->id_kyma;
			$foto->status = 1;
			
			$foto->save();
			
			$this->redirect(array('view','id'=>$person->id));
		}

		$this->render('foto',array(
			'person'=>$person, 'width_real'=>$width_real, 'height_real'=>$height_real,
		));
	}

	public function img_crop($src, $dest, $x, $y, $width, $height, $rgb = 0xFFFFFF, $quality = 100)
	{          
	
	
		//if (!file_exists($src)) {   
		//print_r('777');
		//	return false;       
		//}         
		$size = getimagesize($src);  
        if ($size === false) {  
			return false;     
		}         
		$format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1)); 
		$icfunc = 'imagecreatefrom'.$format; 
		if (!function_exists($icfunc)) { 
			return false;      
		}          
		$isrc  = $icfunc($src); 
		$idest = imagecreatetruecolor($width, $height);   
		imagefill($idest, 0, 0, $rgb);       
		imagecopyresampled($idest, $isrc, 0, 0, $x, $y, $width, $height, $width, $height);    
		imagejpeg($idest, $dest, $quality);
		imagedestroy($isrc); 
		imagedestroy($idest); 
		return true; 
	}  


	
	
	public function actionTest()
	{
		$this->layout = "admin";
		//$person=Person::model()->find('id_kyma=:id_kyma', array(':id_kyma'=>$id_kyma));

		/*if(isset($_POST['Person']))
		{
			$model->attributes=$_POST['Person'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}*/


		$this->render('test');
	}

	public function actionViewan($id)//,$id_work,$id_metro)
	{
		//������� �����
		$value = (string)Yii::app()->request->cookies['current'];
        if(!empty($value)) {
		
	
			$current = CJSON::decode($value);
			
	
			//foreach($current as $www){
				$current_obj=Work::model()->find('id=:id', array(':id'=>$current[w]));
				print_r($current_work);
			//}
			$current_work = $current_obj->name;
			$current_work_id = $current_obj->id;
		}else{
			$current_work='0';
		}
		
        $work_array = array();
        $metro_array = array();
		$model = $this->loadModel($id);

		$modeldop=Persondop::model()->find('id_kyma=:id_kyma', array(':id_kyma'=>$model->id_kyma));

		if(!isset($modeldop)){
			$modeldop='1';
		}

        /*$work = Work::model()->findByPk($id_work);
        $metro = Metro::model()->findByPk($id_metro);

        $model->workkor=$work->name;
        $model->id_workkor=$work->id;
        $model->metrokor=$metro->name;
        $model->id_metrokor=$metro->id;*/

        $criteria_work = new CDbCriteria;
        $criteria_work->condition = 'id_person=:id_person';
        $criteria_work->params = array(':id_person' => $model->id);
        $worklink = Worklink::model()->findAll($criteria_work);

        $i=0;
        foreach($worklink as $work_item){
            $criteria_el = new CDbCriteria;
            $criteria_el->condition = 'status=:status and id=:id';
            $criteria_el->params = array(':status' => 1, ':id' =>$work_item->id_work);
            $work_el = Work::model()->find($criteria_el);
            if (isset($work_el)){
				$work_array[$i]=$work_el;
				$i++;
			}
        }

        $criteria_metro = new CDbCriteria;
        $criteria_metro->condition = 'id_person=:id_person';
        $criteria_metro->params = array(':id_person' => $model->id);
        $metrolink = Metrolink::model()->findAll($criteria_metro);

        $j=0;
        foreach($metrolink as $metro_item){
            $criteria_el_m = new CDbCriteria;
            $criteria_el_m->condition = 'id=:id';
            $criteria_el_m->params = array(':id' =>$metro_item->id_metro);
            $metro_el = Metro::model()->find($criteria_el_m);
            $metro_array[$j]=$metro_el->name;
            $j++;
        }

		$this->render('viewan',array(
			'model'=>$model,'modeldop'=>$modeldop,'work_array'=>$work_array,'metro_array'=>$metro_array, 'current_work'=>$current_work, 'current_work_id'=>$current_work_id//'work'=>$work,'metro'=>$metro,
		));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->layout = "admin";
		$model =$this->loadModel($id);
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layout = "admin";
		$model=new Person;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Person']))
		{
			$model->attributes=$_POST['Person'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layout = "admin";
		$model=$this->loadModel($id);
		
		$criteria_work = new CDbCriteria;
        $criteria_work->condition = 'id_person=:id_person';
        $criteria_work->params = array(':id_person' => $model->id);
        $worklink = Worklink::model()->findAll($criteria_work);

        $i=0;
        foreach($worklink as $work_item){
            $criteria_el = new CDbCriteria;
            $criteria_el->condition = 'status=:status and id=:id';
            $criteria_el->params = array(':status' => 1, ':id' =>$work_item->id_work);
            $work_el = Work::model()->find($criteria_el);
            $work_array[$i]=$work_el->name;
            $i++;
        }

        $criteria_metro = new CDbCriteria;
        $criteria_metro->condition = 'id_person=:id_person';
        $criteria_metro->params = array(':id_person' => $model->id);
        $metrolink = Metrolink::model()->findAll($criteria_metro);

        $j=0;
        foreach($metrolink as $metro_item){
            $criteria_el_m = new CDbCriteria;
            $criteria_el_m->condition = 'id=:id';
            $criteria_el_m->params = array(':id' =>$metro_item->id_metro);
            $metro_el = Metro::model()->find($criteria_el_m);
            $metro_array[$j]=$metro_el->name;
            $j++;
        }
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$modeldop=Persondop::model()->find('id_kyma=:id_kyma', array(':id_kyma'=>$model->id_kyma));
		
		if(empty($modeldop)){
			$modeldop = new Persondop;
		}
		
		if(isset($_POST['Persondop']))
		{
			$modeldop->attributes=$_POST['Persondop'];
			$modeldop->id_kyma = $model->id_kyma;
			$modeldop->status = 1;
			
			if($modeldop->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,'modeldop'=>$modeldop, 'work_array'=>$work_array,'metro_array'=>$metro_array,
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Person');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layout = "admin";

		$criteria=new CDbCriteria;
        $name_education=Education::model()->findAll($criteria);

		$model=new Person('search');
		//$newfoto=Newfoto::model()->find('id_kyma=:id_kyma', array(':id_kyma'=>$model->id_kyma));
		
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Person']))
			$model->attributes=$_GET['Person'];

		$this->render('admin',array(
			'model'=>$model,
			'name_education'=>$name_education,
		));
	}


	public function loadModel($id)
	{
		$model=Person::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='person-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
