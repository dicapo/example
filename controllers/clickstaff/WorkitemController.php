<?php
class WorkitemController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','searchf','podbor','korz','korzdelitem','korzdel'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','create','update','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(''),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionSearchf($work=0,$metro=0,$year_ot=0,$year_do=0)
    {
        $this->processPageRequest('page');

        $criteria = new CDbCriteria;
        $items = new Person;
		$criteria = $items->search_wmy($work,$metro,$year_ot,$year_do);
		//$criteria->condition = 'status=:status';
        //$criteria->params = array(':status' => 1);
		
		
		$count = Person::model()->count($criteria);
		$pages=new CPagination($count);
		$pages->pageSize=8;
		$pages->applyLimit($criteria);

        $items=Person::model()->findAll($criteria);

        $filtr_text = '';
        if($work!=0) {
            $work_name_ob=Work::model()->findByPk($work);
            if($filtr_text!=''){$filtr_text.='/ ';}
            $filtr_text.= $work_name_ob->name;
        } else {
            $work_name_ob = NULL;
        }

        if($metro!=0) {
            $criteria_work = new CDbCriteria;
            $criteria_work->condition = 'id=:id';
            $criteria_work->params = array(':id' => $metro);
            $metro_name_ob = Metro::model()->find($criteria_work);
            if($filtr_text!=''){$filtr_text.='/ ';}
            $filtr_text.= $metro_name_ob->name;
        } else {
            $metro_name_ob = NULL;
        }

        if($year_ot!=0) {
            if($filtr_text!=''){$filtr_text.='/ ';}
            $filtr_text.= 'от '.$year_ot;
        }

        if($year_do!=0) {
            if($filtr_text!=''){$filtr_text.='/ ';}
            $filtr_text.= 'до '.$year_do;
        }

        if (Yii::app()->request->isAjaxRequest){
            $this->renderPartial('_viewloop', array(
                'items'=>$items,'work_name_ob'=>$work_name_ob, 'metro_name_ob'=>$metro_name_ob,  'filtr_text'=>$filtr_text, 'pages'=>$pages,
            ));
            Yii::app()->end();
        } else {
            $this->render('searchf', array(
                'items'=>$items, 'work_name_ob'=>$work_name_ob, 'metro_name_ob'=>$metro_name_ob,  'filtr_text'=>$filtr_text, 'pages'=>$pages,
            ));
        }
		
		/*$this->render('test');*/
		
		
    }

    protected function processPageRequest($param='page')
    {
        if (Yii::app()->request->isAjaxRequest && isset($_POST[$param]))
            $_GET[$param] = Yii::app()->request->getPost($param);
    }

 	public function actionSearchf1($work=0,$metro=0,$year=0)
	{

        $criteria=new CDbCriteria;
        $criteria->condition='id=:id';
        $criteria->params=array(':id'=>$work);
        $works=Work::model()->find($criteria);

      //  print_r(count($works).'qqqqq');

        $criteria_p=new CDbCriteria;
        $criteria_p->condition='id_work=:id_work';
        $criteria_p->params=array(':id_work'=>$work);
        $persons=Worklink::model()->findAll($criteria_p);

     //  print_r(count($persons).'aaaaa');

        if($metro!=0){}
        if($year!=0){}


        if(!empty($persons)) {
            $i = 0;
            foreach ($persons as $person) {
                $items[$i] = Person::model()->findByPk($person->id_person);
                $i++;
            }
        }else{
            $items = 0;
        }


    // print_r(count($items).'cccccc');


		$this->render('searchf',array(
			'work'=>$works, 'metro'=>$metro, 'year'=>$year, 'items'=>$items,
		));
    }

    public function actionPodbor($id) 
	{
        $nove=array();
        $value = (string)Yii::app()->request->cookies['qwer'];

		//выбраная анкета 
        $model = new Ank;
        $model->id = $id;
		$current_str = (string)Yii::app()->request->cookies['current'];

		$current = CJSON::decode($current_str);
		
		if(!empty($current)) {
			$model->id_work = $current[w];
        } else {
			$model->id_work = 0;
		}
		$nove[0] = $model;

		//выбор из cookie анкет
		$i=1;
        if(!empty($value)) {
                $per = CJSON::decode($value);

                foreach ($per as $rr) {
                    $model = new Ank;
                    $model->id = $rr['id'];
					$model->id_work = $rr['id_work'];
                    $nove[$i] = $model;
                    $i++;
                }
                $persid = CJSON::encode($nove);
                $model->setCookie($persid);
        }else{
             $persid = CJSON::encode($nove);
			 $model->setCookie($persid);
        }
        $this->redirect(Yii::app()->request->urlReferrer);
    }

    public function actionKorz()
	{
        $i=0;
        $value = (string)Yii::app()->request->cookies['qwer'];
        
		if(!empty($value)) {
            $per = CJSON::decode($value);
            foreach($per as $rr){
             //  $model = new Ank;
             //   $model->id = $rr['id'];
				//if($rr['id_work']!=0){
				//	$work = Work::model()->findByPk($rr['id_work']);
				//}else{
				//	$work = 0;
				//}	
				
				//print_r($rr['id_work']);
				/*if($rr['id_metro']!=0){	
					$metro = Metro::model()->findByPk($rr['id_metro']);
				}else{
					$metro = 0;
				}	*/
                //$criteria=new CDbCriteria;
                //$criteria->condition='id=:id';
                //$criteria->params=array(':id'=>$rr['id_work']);
                //$work=Work::model()->find($criteria);

                $criteria=new CDbCriteria;
                $criteria->condition='id=:id';
                $criteria->params=array(':id'=>$rr['id']);
                $person=Person::model()->find($criteria);
                
				//все вакансии
				$criteria_work = new CDbCriteria;
				$criteria_work->condition = 'id_person=:id_person';
				$criteria_work->params = array(':id_person' => $person->id);
				$worklink = Worklink::model()->findAll($criteria_work);
				$work_array=array();
				$ii=0;
				foreach($worklink as $work_item){
					$criteria_el = new CDbCriteria;
					$criteria_el->condition = 'status=:status and id=:id';
					$criteria_el->params = array(':status' => 1, ':id' =>$work_item->id_work);
					$work_el = Work::model()->find($criteria_el);
					if (isset($work_el)){
						$work_array[$ii]=$work_el->name;
						$ii++;
					}
				}
				$person->workarray = $work_array;//все вакансии
				
				
				//все метро
				$criteria_metro = new CDbCriteria;
				$criteria_metro->condition = 'id_person=:id_person';
				$criteria_metro->params = array(':id_person' => $person->id);
				$metrolink = Metrolink::model()->findAll($criteria_metro);
				$metro_array=array();
				$j=0;
				foreach($metrolink as $metro_item){
					$criteria_el_m = new CDbCriteria;
					$criteria_el_m->condition = 'id=:id';
					$criteria_el_m->params = array(':id' =>$metro_item->id_metro);
					$metro_el = Metro::model()->find($criteria_el_m);
					$metro_array[$j]=$metro_el->name;
					$j++;
				}
				$person->metroarray = $metro_array;//все метро
				
				
				
				
				//$person->metroarray = $person->getmetro($person->metrofn);
			//	foreach($person->metroarray as $rr){
				//print_r($person->metroarray);
			//	}
			
				
				if($rr['id_work']!=0){
					$person->workkor = $work->name;
				}else{
					$person->workkor = 0;
				}	
				/*if($rr['id_metro']!=0){	
					$person->metrokor = $metro->name;
				}else{
					$person->metrokor = 0;
				}	*/
                
				$nove[$i] = $person;
                $i++;
            }

			
			//отправка выбора на почту
			$model = new Otprav;
			//print_r($_POST.'123');
			//$this->performAjaxValidation($model);
			if(isset($_POST['fio']))
			{
				//$model->attributes=$_POST['Otprav'];
				$model->fio = $_POST['fio'];
				$model->tel = $_POST['tel'];
				$model->email = $_POST['Otprav']['email'];
				$model->text = $_POST['Otprav']['text'];
				
				  //if($model->validate()){
						//if($model->save())
						//print_r('8888');
						
						$message ='';
						$message.='Имя: ';
						$message.=$model->fio.'<br/>';
						$message.='Телефон: ';
						$message.=$model->tel.'<br/>';
						$message.='Email: ';
						$message.=$model->email.'<br/>';
						$message.='Дополнительный текст<br/>';
						$message.=$model->text.'<br/>';
						$message.='Данные выбранных анкет<br/>';
						
						foreach($nove as $nov){
							$message.='ID - ';
							$message.=$nov->id_kyma.'<br/>';
							$message.='вакансия - ';
							$message.=$nov->workkor.'<br/>';
						    $message.='метро - ';
							$message.=$nov->metrokor.'<br/>';
							$message.='<hr/>';
						}
					
						$to = "info@clickstaff.ru,";
						$to.= "clickstaff@mail.ru,";
						$to.= "fizinta@yandex.ru";
						$headers  = "Content-type: text/html; charset=UTF-8 \r\n";
					
					mail ($to,
						  "Заявка из корзины - ваш выбор",
						  $message,
						  $headers);      
						  
					//$this->redirect(Yii::app()->user->returnUrl); 
					//$this->redirect(array('site/index'));
					//}
			}
			
			$test = new Person; 
			
            $this->render('spisok',array(
                'nove'=>$nove,		//
				//'model'=>$model,	//???
				'test'=>$test,      //для работы функции скланения Соискателя
            ));

        }else{
            $this->render('null_basket');
        }

    }

    public function actionKorzdelitem($id)
    {
        $value = (string)Yii::app()->request->cookies['qwer'];
        if(!empty($value)) {
            $i=0;
            $per = CJSON::decode($value);
            foreach($per as $rr){
                $model = new Ank;
                $model->id = $rr['id'];
                $model->id_work = $rr['id_work'];
				$model->id_metro = $rr['id_metro'];
                if($i!=$id) $nove[$i] = $model;
                $i++;
            }
            $modelr = new Ank;
            if(!empty($nove)){
                $persid = CJSON::encode($nove);
                $modelr->setCookie($persid);
            }else{

                unset(Yii::app()->request->cookies['qwer']);


            }
        }



        $this->redirect(Yii::app()->request->urlReferrer);
    }
    public function actionKorzdel()
    {
        unset(Yii::app()->request->cookies['qwer']);
        $this->redirect(Yii::app()->getHomeUrl());
    }

	public function actionView($chpu)
	{
		$this->processPageRequest('page');
		
		$model = Workitem::model()->find('chpu=:chpu', array(':chpu'=>$chpu));

		//запись текущего положения в cookie
		$cur = new Choice;
		$cur->w = $model->id_work;
		$cur_str = CJSON::encode($cur);
		$cur->setCookie($cur_str);
		
        $work = Work::model()->findByPk($model->id_work);

		$criteria=new CDbCriteria;
		//$criteria->order = ' RAND()';
		$criteria->condition = 'id_work=:id_work';
		$criteria->params = array(':id_work' => $model->id_work);
		$criteria->with = array('workfn' => array('together' => true));
		
		$count = Person::model()->count($criteria);
		$pages=new CPagination($count);
		$pages->pageSize=8;
		$pages->applyLimit($criteria);
		
		$persons=Person::model()->findAll($criteria);

		
        if (Yii::app()->request->isAjaxRequest){
            $this->renderPartial('_viewloopmenu', array(
                'items'=>$persons, 'work'=>$work, 'model'=>$model, 'pages'=>$pages,
            ));
            Yii::app()->end();
        } else {
            $this->render('view', array(
                'items'=>$persons, 'work'=>$work, 'model'=>$model, 'pages'=>$pages,
            ));
        }
		

	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        $this->layout = "admin";

        $criteria=new CDbCriteria;

        $criteria->condition='status=:status';
        $criteria->params=array(':status'=>1);

        $name_work=Work::model()->findAll($criteria);

        $model=new Workitem;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Workitem']))
		{
			$model->attributes=$_POST['Workitem'];
            $model->id_work = $_POST['Workitem']['name_work'];
			$model->chpu = $model->chputitle($model->title);
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,'name_work'=>$name_work,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $this->layout = "admin";

        $criteria=new CDbCriteria;

        $criteria->condition='status=:status';
        $criteria->params=array(':status'=>1);

        $name_work=Work::model()->findAll($criteria);
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Workitem']))
		{
			$model->attributes=$_POST['Workitem'];
            $model->id_work = $_POST['Workitem']['name_work'];
			$model->chpu = $model->chputitle($model->title);
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,'name_work'=>$name_work,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Workitem');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        $this->layout = "admin";
		$model=new Workitem('search');

        $criteria=new CDbCriteria;
        $criteria->condition='status=:status';
        $criteria->params=array(':status'=>1);

        $name_work=Work::model()->findAll($criteria);

		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Workitem']))
			$model->attributes=$_GET['Workitem'];

		$this->render('admin',array(
			'model'=>$model,'name_work'=>$name_work,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Workitem::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) )
		{
		  //print_r('777');
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}else{
		  // print_r('888');
		}
	}
}
