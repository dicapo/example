

<div class="book-back t-text-tour"><a href="<?php echo Yii::app()->request->baseUrl; ?>/berlin">< Back</a></div>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'book-form-i',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<?php if($form->errorSummary($contact)!=''){ ?>
<div class="book-guide" style="margin-bottom:20px;color:red;padding:20px;"> 
	<?php echo $form->errorSummary($contact); ?>
</div>
<?php } ?>
<div class="book-guide">
     
    <div class="book-foto">
        <div class="book-guide-foto"><img src="<? echo Yii::app()->request->baseUrl; ?>/image/guide/<? echo $scheduled->user_ob->guide_ob->lnk_to_picture;?>"/></div>
        <div class="book-guide-name t-guide-name"><? echo $scheduled->user_ob->contact_ob->firstname;?></div>
    </div>

    <div class="book-col1">
        <div class="book-input">
            <div class="book-f">City</div>
       	    <div class="book-cdt"><? echo $scheduled->city_ob->seg_cityname; ?></div>
            <?php echo $form->hiddenField($contact,'city_ex',array('value'=>$scheduled->city_ob->seg_cityname)); ?>
            <div style="clear: both;"></div>
        </div>
        <div class="book-input">
             <div class="book-f">Date</div>
       	    <div class="book-cdt"><? echo date('d/m/Y',$scheduled->date_now); ?></div>
            <?php echo $form->hiddenField($contact,'date_ex',array('value'=>$scheduled->date_now)); ?>
            <div style="clear: both;"></div>
        </div>
        <div class="book-input">
             <div class="book-f">Time</div>
              <? $time_format = substr_replace($scheduled->starttime, 0, 4);?>
       	    <div class="book-cdt"><? echo $time_format; ?></div>
            <?php echo $form->hiddenField($contact,'time_ex',array('value'=>$scheduled->starttime)); ?>
            <div style="clear: both;"></div>
        </div>
    </div>

    <div class="book-col2">
        <div class="book-input">
            <div class="book-f">Tour</div>
            
           
            
			 <? if($scheduled->tourroute_id==null){?>
                 <div class="book-select">
                  <?php $list = CHtml::listData($tours_guide, 'idseg_tourroutes', 'name'); ?>
                  <?php echo $form->dropDownList($contact,'tour',$list, array('options' => array($cat_item=>array('selected'=>true)))); ?>
                 </div>
             <? } else {?>
                <div class="book-cdt"><? echo $scheduled->tourroute_ob->name; ?></div>
                <?php echo $form->hiddenField($contact,'tour',array('value'=>$scheduled->tourroute_ob->idseg_tourroutes)); ?>
             <? }?>
             
             <div style="clear: both;"></div>
        </div>
        <div class="book-input">
            <div class="book-f">Language</div>
            <? if($scheduled->language_id==null){?>	
                <div class="book-select">
                      <?php $list_l = CHtml::listData($languages_guide, 'id_languages', 'englishname'); ?>
                      <?php echo $form->dropDownList($contact,'language',$list_l); ?>
                </div>
            <? } else {?>
            	<div class="book-cdt"><? echo $scheduled->language_ob->englishname; ?></div>
                <?php echo $form->hiddenField($contact,'language',array('value'=>$scheduled->language_ob->id_languages)); ?>
            <? }?>
            <div style="clear: both;"></div>
        </div>
        <div class="book-input">
            <div class="book-f">Tickets</div>
       	    <div class="book-select">
                  <?php $list_k = array('1'=>1,'2'=>2,'3'=>3,'4'=>4,'5'=>5,'6'=>6,'7'=>7,'8'=>8,'9'=>9,'10'=>10); ?>
		          <?php echo $form->dropDownList($contact,'tickets',$list_k, array('id'=>'area', 'onChange'=>'qwe(this.value)')); ?>
            </div>
            <div style="clear: both;"></div>
        </div>
    </div>

    
    
        <div class="tour_price_guide">
            <div class="t-evro-tour" style="width:95px;border-top: 1px dotted #a7a7a7;border-left:1px dotted #a7a7a7;border-right:1px dotted #a7a7a7;">
            	<div id="price" style="display:none;"><?php echo $tour->base_price;?></div>
                <div id="new_price" style="float:left;padding-left:20px;">
					<?php echo $tour->base_price;?>
                </div>    
                <div style="float:left;padding-left:10px;">
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/img/str2/evro.png" />
                </div>
                <div style="clear:both;"></div>
            </div>
            <div class="t-vat-tour" style="padding-bottom:10px;width:95px;border-bottom: 1px dotted #a7a7a7;border-left:1px dotted #a7a7a7;border-right:1px dotted #a7a7a7;">Incl.VAT</div>
        </div>



    <div style="clear: both;"></div>
 
 
 
     
     
</div>



<div class="book-contact">
<div class="book-contact-name">
	Booking Form
</div>
    <div class="book-form">
        <div class="book_form-item">
            <div class="book-f">First name</div>
            <div class="book-field"><?php echo $form->textField($contact,'firstname',array('placeholder'=>'John')); ?>
           <!-- <php echo $form->error($contact,'firstname'); ?>-->
            
            </div>
            
             <div style="clear: both;"></div>
        </div>
        <div class="book_form-item">
            <div class="book-f">Last name</div>
            <div class="book-field"><?php echo $form->textField($contact,'lastname',array('placeholder'=>'Llvingstone')); ?></div>
             <div style="clear: both;"></div>
        </div>
        <div class="book_form-item">
            <div class="book-f">Address</div>
            <div class="book-field"><?php echo $form->textArea($contact,'address',array('placeholder'=>'Two East 55th Street, at Fifth Avenue; New York 10022; Unlted States')); ?></div>
             <div style="clear: both;"></div>
        </div>
    </div>
    <div class="book-form">
        <div class="book_form-item">
            <div class="book-f">City</div>
            <div class="book-field"><?php echo $form->textField($contact,'city',array('placeholder'=>'New York')); ?></div>
             <div style="clear: both;"></div>
        </div>
        <div class="book_form-item">
            <div class="book-f">Country</div>
            <div class="book-field"><?php echo $form->textField($contact,'country',array('placeholder'=>'Unlted States')); ?></div>
             <div style="clear: both;"></div>
        </div>
        <div class="book_form-item">
            <div class="book-f">Phone</div>
            <div class="book-field"><?php echo $form->textField($contact,'phone',array('placeholder'=>'(1)(___)___-___')); ?></div>
             <div style="clear: both;"></div>
        </div>
        <div class="book_form-item">
            <div class="book-f">E-mail</div>
            <div class="book-field"><?php echo $form->textField($contact,'email',array('placeholder'=>'johnllvingstone@gmail.com')); ?></div>
             <div style="clear: both;"></div>
        </div>
    </div>
    <div style="clear: both;"></div>
</div>
<button class="but-book" type="submit"><?php echo 'BOOK'; ?></button>

<?php $this->endWidget(); ?>   
<script type="text/javascript">
	window.onload = function () {
  		var price_base = document.getElementById('price').innerHTML;
		var c = document.getElementById('area').value*price_base;
		document.getElementById('new_price').innerHTML=c;
  	}

	function qwe(area_id){


		//document.getElementById('price').value = 23;
		var c = document.getElementById('price').innerHTML*area_id;
		document.getElementById('new_price').innerHTML=c;
		}
</script>