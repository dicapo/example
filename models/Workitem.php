<?php
class Workitem extends CActiveRecord
{
	public function tableName()
	{
		return '{{workitem}}';
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('chpu,title', 'required'),
			array('chpu', 'unique'),
			array('id_work, created', 'numerical', 'integerOnly'=>true),
			array('chpu', 'length', 'max'=>100),
			array('name', 'length', 'max'=>45),
			array('title, description, keywords', 'length', 'max'=>300),
			array('text', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_work, chpu, name_work, name, text, title, description, keywords, created', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'name_work' => array(self::BELONGS_TO, 'Work', 'id_work'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_work' => 'Id Work',
			'chpu' => 'Человекопонятные URL (латиница)',
			'name_work' => 'Категория',
			'name' => 'Наименование',
			'text' => 'Описание',
			'title' => 'Title',
			'description' => 'Description',
			'keywords' => 'Keywords',
			'created' => 'Created',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('id_work',$this->id_work);
		//$criteria->compare('idWork',$this->name_work,true);
		$criteria->compare('chpu',$this->chpu,true);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('keywords',$this->keywords,true);
		$criteria->compare('created',$this->created);

        $criteria->with = array('name_work');
        $criteria->compare('name_work.id',$this->name_work);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'id_work ASC',
            ),
		));
	}

	public function chputitle($string )
	{
		static $lang2tr = array(
			// russian
			'й'=>'j','ц'=>'c','у'=>'u','к'=>'k','е'=>'e','н'=>'n','г'=>'g','ш'=>'sh',
			'щ'=>'sh','з'=>'z','х'=>'h','ъ'=>'','ф'=>'f','ы'=>'y','в'=>'v','а'=>'a',
			'п'=>'p','р'=>'r','о'=>'o','л'=>'l','д'=>'d','ж'=>'zh','э'=>'e','я'=>'ja',
			'ч'=>'ch','с'=>'s','м'=>'m','и'=>'i','т'=>'t','ь'=>'','б'=>'b','ю'=>'ju','ё'=>'e','и'=>'i',

			'Й'=>'J','Ц'=>'C','У'=>'U','К'=>'K','Е'=>'E','Н'=>'N','Г'=>'G','Ш'=>'SH',
			'Щ'=>'SH','З'=>'Z','Х'=>'H','Ъ'=>'','Ф'=>'F','Ы'=>'Y','В'=>'V','А'=>'A',
			'П'=>'P','Р'=>'R','О'=>'O','Л'=>'L','Д'=>'D','Ж'=>'ZH','Э'=>'E','Я'=>'JA',
			'Ч'=>'CH','С'=>'S','М'=>'M','И'=>'I','Т'=>'T','Ь'=>'','Б'=>'B','Ю'=>'JU','Ё'=>'E','И'=>'I',
			// czech
			'á'=>'a', 'ä'=>'a', 'ć'=>'c', 'č'=>'c', 'ď'=>'d', 'é'=>'e', 'ě'=>'e',
			'ë'=>'e', 'í'=>'i', 'ň'=>'n', 'ń'=>'n', 'ó'=>'o', 'ö'=>'o', 'ŕ'=>'r',
			'ř'=>'r', 'š'=>'s', 'Š'=>'S', 'ť'=>'t', 'ú'=>'u', 'ů'=>'u', 'ü'=>'u',
			'ý'=>'y', 'ź'=>'z', 'ž'=>'z',

			'і'=>'i', 'ї' => 'i', 'b' => 'b', 'І' => 'i',
			// special
			' '=>'_', '\''=>'', '"'=>'', '\t'=>'', '«'=>'', '»'=>'', '?'=>'', '!'=>'', '*'=>'', '-'=>'_'
		);
		$url = preg_replace( '/[\-]+/', '-', preg_replace( '/[^\w\-\*]/', '', strtolower( strtr( $string, $lang2tr ) ) ) );
		//echo $url."<br>";
		return  $url;
	
	
	
		//$r = array('а','б','в','г','д','е','ё','ж','з','и','й','к','л','м', 'н','о','п','р','с','т','у','ф','х','ц','ч', 'ш', 'щ', 'ъ','ы','ь','э', 'ю', 'я',' ');
		//$l = array('a','b','v','g','d','e','e','g','z','i','y','k','l','m','n', 'o','p','r','s','t','u','f','h','c','ch','sh','sh','', 'y','y', 'e','yu','ya','-');
		//$s = str_replace( $r, $l, strtolower($s) );
		//$s = preg_replace("/[^\w\-]/","$1",$s);
		//$s = preg_replace("/\-{2,}/",'-',$s);
		//return trim($s,'-');
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
