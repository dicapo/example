<?php
class Person extends CActiveRecord
{
    public $workkor; /*текущая вакансия*/
    public $id_workkor; /*id текущей вакансии*/
    public $metrokor; /*текущее метро*/
    public $id_metrokor; /*id текущего метро*/
	
	public $metroarray=array(); /*все метро*/
    public $workarray; /*все вакансии*/
	
    // public $name_work;
	public $image;

	public function tableName()
	{
		return '{{person}}';
	}

	public function rules()
	{
		return array(
			//array('id_kyma, work', 'required'),
			array('id_kyma, metro, work, age, education, reiting, created', 'numerical', 'integerOnly'=>true),
			array('stag, nationality, file_name', 'length', 'max'=>45),
			array('navik', 'length', 'max'=>300),
			array('image', 'file',
				'types'=>'jpg,JPG',
				'maxSize'=>1024 * 1024 * 1, // 1МВ
				'allowEmpty'=>'true',
				'tooLarge'=>'Ваш файл больше 1 МВ',
			),
			array('id, id_kyma, metro, name_metro, work, name_work, age, education, name_education, stag, nationality, navik, reiting, file_name, image, created, newfoto', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'name_education' => array(self::BELONGS_TO, 'Education','education'),
            'dop' => array(self::BELONGS_TO, 'Persondop','id_kyma'),
			'newfoto' => array(self::HAS_ONE, 'Newfoto','id_person'),
			'workfn' => array(self::HAS_ONE, 'Worklink', 'id_person'), //объект id всех вакансий
            'metrofn' => array(self::HAS_ONE, 'Metrolink', 'id_person'), //объект id всех метро
			
			'name_metro' => array(self::BELONGS_TO, 'Metro','metro'),
			'name_work' => array(self::BELONGS_TO, 'Work','work'),
			

		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_kyma' => 'ID анкеты БД Кума',
			'metro' => 'Метро',
			'name_metro' => 'Метро',
			'work' => 'Вакансии',
			'name_work' => 'Вакансии',
			'age' => 'Возраст',
			'education' => 'ID образования',
			'name_education' => 'Образование',
			'stag' => 'Стаж',
			'nationality' => 'Национальность',
			'navik' => 'Умение и навык',
			'reiting' => 'Рейтинг',
			'file_name' => 'Название файла фото',
			'image' => 'Фото',
			'created' => 'Дата создания анкеты',
			'newfoto' => 'Фото',
		);
	}

	//преобразования объектов вакансия и метро
	public function getwork($workfn)
	{
		$i=0;
		$workarray = NULL;
		if(!empty($workfn)){
			foreach($workfn as $item){
				$criteria = new CDbCriteria;
				$criteria->condition = 'status=:status and id=:id';
				$criteria->params = array(':status' => 1, ':id' =>$item);
				$work_el = Work::model()->find($criteria);
				if (isset($work_el)){
					$workarray[$i]=$work_el;
					$i++;
				}
			}
		}
		return $workarray;
	}

	public function getmetro($metrofn)
	{
	print_r($metrofn);
		$i=0;
		$metroarray = array();
		if(!empty($metrofn)){
			foreach($metrofn as $items){
				//foreach($items as $item){
				
				//print_r('v');
					$criteria = new CDbCriteria;
					$criteria->condition = 'id=:id';
					$criteria->params = array(':id' =>$items->id_metro);
					$metro_el = Metro::model()->find($criteria);
					if (isset($metro_el)){
						$metroarray[$i]=$metro_el->name;
						$i++;
					}
				//}
			}
		}
		return $metroarray;
	}
	
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('id_kyma',$this->id_kyma);
		$criteria->compare('metro',$this->metro);
		$criteria->compare('work',$this->work);
		$criteria->compare('age',$this->age);
		$criteria->compare('education',$this->education);
		$criteria->compare('stag',$this->stag);
		$criteria->compare('nationality',$this->nationality);
		$criteria->compare('navik',$this->navik);
		$criteria->compare('reiting',$this->reiting);
		$criteria->compare('file_name',$this->file_name,true);
		$criteria->compare('created',$this->created);

		//$criteria->with = array('name_metro');
		//$criteria->compare('name_metro.id', $this->name_metro,true);

		//$criteria->with = array('name_work');
		//$criteria->compare('name_work.id', $this->name_work,true);

	
		
		$criteria->with = array('name_education','newfoto');
		//$criteria->with = array('newfoto');
		$criteria->compare('name_education.id', $this->name_education);
		
		$criteria->compare('newfoto.status', $this->newfoto,true);

       // $criteria->with = array('workfn');
       // $criteria->compare('workfn.id', $this->workfn,true);

      //  $criteria->with = array('metrofn');
      //  $criteria->compare('metrofn.id', $this->metrofn,true);

        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function search_wmy($work, $metro, $year_ot, $year_do)
    {
        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('id_kyma',$this->id_kyma);
        $criteria->compare('metro',$this->metro);
        $criteria->compare('work',$this->work);
        $criteria->compare('age',$this->age);
        $criteria->compare('education',$this->education);
        $criteria->compare('stag',$this->stag,true);
        $criteria->compare('nationality',$this->nationality,true);
        $criteria->compare('navik',$this->navik,true);
        $criteria->compare('reiting',$this->reiting);
        $criteria->compare('file_name',$this->file_name,true);
        $criteria->compare('created',$this->created);

        if($metro!=0) {
			if($work!=0){
				$criteria->with = array('workfn', 'metrofn' => array('together' => true));
				$criteria->compare('workfn.id_work', $work);
				$criteria->compare('metrofn.id_metro', $metro);
			}else{
				$criteria->with = array('metrofn' => array('together' => true));
				$criteria->compare('metrofn.id_metro', $metro);
			}
        }else{
			if($work!=0){
				$criteria->with = array('workfn' => array('together' => true));
				$criteria->compare('workfn.id_work', $work);
			}
        }

        if($year_ot!=0){
            $criteria->compare('age', '>'.$year_ot);
        }
        if($year_do!=0){
            $criteria->compare('age', '<'.$year_do);
        }
        return $criteria;

    }

    public function search_wmy_dop($work,$id_person)
    {
        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('id_kyma',$this->id_kyma);
        $criteria->compare('metro',$this->metro);
        $criteria->compare('work',$this->work);
        $criteria->compare('age',$this->age);
        $criteria->compare('education',$this->education);
        $criteria->compare('stag',$this->stag,true);
        $criteria->compare('nationality',$this->nationality,true);
        $criteria->compare('navik',$this->navik,true);
        $criteria->compare('reiting',$this->reiting);
        $criteria->compare('file_name',$this->file_name,true);
        $criteria->compare('created',$this->created);

        $criteria->condition = '`t`.`id` <>'.$id_person;

        $criteria->with = array('workfn' => array('together' => true));
        $criteria->compare('workfn.id_work', $work);

        return $criteria;

    }

    public function sklon_num($num, $word) {
        $cases = array (2, 0, 1, 1, 1, 2);
        return $word[ ($num%100 > 4 && $num %100 < 20) ? 2 : $cases[min($num%10, 5)] ];
    }

    public function sklon_text($word) {
        $word_old = array ('Няня','Гувернантка','Домработница','Семейная пара','Водитель','Повар','Сиделка','Уборка');
        $word_new = array ('Няню','Гувернантку','Домработницу','Семейную пару','Водителя','Повара','Сиделку','Уборку');
        $j=0;
        for($i=0;$i<count($word_old);$i++){
            if($word==$word_old[$i]){$j=$i;}
        }
        return $word_new[$j];
    }

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
